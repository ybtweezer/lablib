from typing import Callable, Optional
from dataclasses import dataclass
import lmfit
import numpy as np
from lablib.analysis.expval import ExpVal

@dataclass
class Param:
    """
    Settings for a parameter passed to an `lmfit` model.

    Fields
    ------
    value : float
        Initial value for the Levenberg-Marquardt algorithm.
    min : float = -numpy.inf
        Lower bound constraint for the parameter.
    max : float = +numpy.inf
        Upper bound constraint for the parameter.
    expr : str = None
        Analytical expression to constrain the parameter to the values of other
        parameters.
    brute_step : float = None
    """
    value: Optional[float] = None
    min: float = -np.inf
    max: float = +np.inf
    vary: bool = True
    expr: Optional[str] = None
    brute_step: Optional[float] = None

class ModelBase:
    """
    Abstract type for a model function to fit to.

    Fields
    ------
    MODELSTR : str
        Description of the functional form of the model.
    PARAMS : set[str]
        Set of parameters required to evaluate the model at a point in
        parameter space.
    """
    MODELSTR: str
    PARAMS: set[str]

    def __init__(self):
        pass

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        """
        Model evaluator.
        """
        raise NotImplementedError()

ModelFn = Callable[[lmfit.Parameters, np.ndarray], np.ndarray]
CostFn = Callable[[lmfit.Parameters, ModelFn, ...], np.ndarray]

class Fit1D:
    """
    Simple driver class for the `lmfit.minimizer` model-fitting process.

    Fields
    ------
    model : ModelBase
        Fit to this model. See `help(ModelBase)` for more info.
    init_params : lmfit.Parameters
        lmfit record of initial parameter values.
    fit_result : lmfit.minimizer.MinimizerResult
        Output of the `lmfit.minimizer` call.
    """
    model: ModelBase
    init_params: lmfit.Parameters
    fit_result: lmfit.minimizer.MinimizerResult

    def __init__(self, model: ModelBase, init_params: dict[str, Param]):
        """
        Construct a new model-fitting driver.

        Parameters
        ----------
        model : ModelBase
            Fit to this model. See `help(ModelBase)` for more info.
        init_params : dict[str, Param]
            Dictionary mapping parameter names to initial settings. The
            parameter names contained here are checked against those declared
            by the model.
        """
        self.model = model
        model_keys = set(self.model.PARAMS)
        missing_keys = {k for k in init_params.keys() if k not in model_keys}
        if len(missing_keys) > 0:
            raise ValueError(f"missing keys: {missing_keys}")
        self.init_params = lmfit.Parameters()
        for (name, settings) in init_params.items():
            self.init_params.add(
                name,
                value=settings.value,
                min=settings.min,
                max=settings.max,
                vary=settings.vary,
                expr=settings.expr,
                brute_step=settings.brute_step,
            )
        self.fit_result = None

    def set_model(self, model: ModelBase):
        """
        Set a new model to fit to.

        Returns `self` afterward.
        """
        self.model = model
        return self

    def set_params(self, params: dict[str, Param]):
        """
        Set new initial parameters.

        Returns `self` afterward.
        """
        for (name, settings) in params.items():
            self.params.add(
                name,
                value=settings.value,
                min=settings.min,
                max=settings.max,
                vary=settings.vary,
                expr=settings.expr,
                brute_step=settings.brute_step,
            )
        return self

    def get_init_params(self) -> lmfit.Parameters:
        """
        Get an `lmfit` record of the initial parameters.

        This is equivalent to direct access of the `init_params` field.
        """
        return self.init_params

    def do_fit(
        self,
        costf: CostFn,
        costf_args: tuple[...],
        *minimizer_args,
        **minimizer_kwargs,
    ):
        """
        Perform the Levenberg-Marquardt fit. Raises `lmfit.MinimizerException`
        if the fit is unseccessful.

        Returns `self` afterward.

        Parameters
        ----------
        costf : CostFn
            Cost function to pass to the lmfit routine. This function must have
            the signature
                costf(lmfit.Parameters, ModelFn, ...) -> numpy.ndarray
            where ModelFn is a function with the signature
                model_fn(lmfit.Parameters, numpy.ndarray) -> numpy.ndarray
        costf_args : tuple[...]
            Tuple of extra arguments to pass to the cost function.
        *minimizer_args : ...
            Extra positional arguments to pass to `lmfit.minimize`.
        **minimizer_kwargs : ...
            Extra keyword arguments to pass to `lmfit.minimze`.
        """
        fit_result = lmfit.minimize(
            costf,
            self.init_params,
            args=(self.model.f, *costf_args),
            *minimizer_args,
            **minimizer_kwargs,
        )
        # if not fit_result.success:
        #     raise lmfit.MinimizerException("fit failed")
        self.fit_result = fit_result
        return self

    def is_fit(self) -> bool:
        """
        Return `True` if a successful fit has been performed.
        """
        return self.fit_result is not None

    def get_fit_result(self) -> Optional[lmfit.minimizer.MinimizerResult]:
        """
        Get the bare `lmfit.minimizer.MinimizerResult` returned by
        `lmfit.minimize` if a successful fit has been performed.

        This is equivalent to direct access of the `fit_result` field.
        """
        return self.fit_result

    def get_result_param(self, key: str) -> ExpVal:
        """
        Get the value and standard error of a single fit parameter as an
        `ExpVal`.

        See `help(libscratch.analysis.ExpVal)` for more info.

        Raises `RuntimeError` if a successful fit has not been performed, or
        `ValueError` if `key` is not a valid parameter name for the model.
        """
        if self.fit_result is None:
            raise RuntimeError("model has not been fit")
        elif key not in self.model.PARAMS:
            raise ValueError(f"invalid key {key}")
        else:
            return ExpVal(
                self.fit_result.params[key].value,
                self.fit_result.params[key].stderr,
            )

    def get_result_params(self) -> dict[str, ExpVal]:
        """
        Get the values and standard errors of all fit parameters as `ExpVal`s.

        See `help(libscratch.analysis.ExpVal)` for more info.

        Raises `RuntimeError` if a successful fit has not been performed.
        """
        return {key: self.get_result_param(key) for key in self.model.PARAMS}

    def get_result_params_arr(self) -> dict[str, np.ndarray]:
        """
        Get the values and standard errors of all fit parameters as two-element
        1D arrays.

        Raises `RuntimeError` if a successful fit has not been performed.
        """
        return {
            key: np.array([ev.val, ev.err])
            for (key, ev) in self.get_result_params().items()
        }

    def gen_paramstr(
        self,
        comment: Optional[str]=None,
        units: Optional[dict[str, str]]=None,
        with_modelstr: bool=True,
        latex_math: bool=True,
    ) -> str:
        """
        Generate a single multiline string giving information on the model
        function and its fit parameter values.

        Raises `RuntimeError` if a successful fit has not been performed.

        Parameters
        ----------
        comment : Optional[str] = None
            Optional notes to attach to the end of the string.
        units : Optional[dict[str, str]] = None
            Optional units to attach to all parameters.
        with_modelstr : bool = True
            Include a string representation of the model function.
        latex_math : bool = True
            Wrap parameters in '$' for latex math rendering.
        """
        if self.fit_result is None:
            raise RuntimeError("model has not been fit")
        units = dict() if units is None else units
        rp = self.get_result_params()
        m = lambda s: ("$" + s + "$") if latex_math else s
        return (
            ((m(self.model.MODELSTR) + "\n") if with_modelstr else "")
            + "\n".join(
                (
                    f"{m(name)} = {param.value_str(latex=latex_math)}"
                    if param.err is not None
                    else f"{m(name)} = {f'{param.err:.5f}'}(nan)"
                ) + " " + units.get(name, "")
                for (name, param) in rp.items()
            )
            + ((comment + "\n") if comment is not None else "")
        )

    def gen_init_params_curve(self, x: np.ndarray) -> np.ndarray:
        """
        Sample the model with initial parameter values.
        """
        return self.model.f(self.init_params, x)

    def gen_fit_curve(self, x: np.ndarray) -> np.ndarray:
        """
        Sample the resulting fit curve.

        Raises `RuntimeError` if a successful fit has not been performed.
        """
        if self.fit_result is None:
            raise RuntimeError("model has not been fit")
        else:
            return self.model.f(self.fit_result.params, x)

