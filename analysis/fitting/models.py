from dataclasses import dataclass
from typing import Callable
import numpy as np
import lmfit
from .fitdefs import ModelBase

GAUSSIAN_STR = "$A * e^{\\frac{(f-f_0)^2}{s^2}} + B$"
LORENTZIAN_STR = "$\\frac{A}{1 + \\frac{(f-f_0)^2}{s^2}} + B$"
LORENTZIAN_STR_2 = "$\\frac{A}{1 + \\frac{(f-f_0)^2}{s^2}} + $" \
    + "$\\frac{A_1}{1 + \\frac{(f-f_1)^2}{s_1^2}} + B$"
LIGHTSHIFT_STR = "$-\\frac{A}{f_0 - f} - \\frac{A}{f_0 + f}$"
LIGHTSHIFT_OFFS_STR = "$-\\frac{A}{f_0 - f} - \\frac{A}{f_0 + f} + B$"
EXPONENTIAL_STR = "$B - Ae^{\\frac{-x}{\\tau}}$"
SINC_STR = "$A \\frac{\\sin(a (x - f_0))}{b (x - f_0)} + B$"
SINC2_STR = "$A \\left(\\frac{\\sin(a (x - f_0))}{b (x - f_0)}\\right)^2 + B$"

def get_paramvals(params: lmfit.Parameters, *keys: str) -> list[float]:
    return [params[k].value for k in keys]

def residuals(
    params: lmfit.Parameters,
    model: Callable[[lmfit.Parameters, np.ndarray], np.ndarray],
    x: np.ndarray,
    y: np.ndarray,
    y_err: np.ndarray,
) -> np.ndarray:
    m = model(params, x)
    y_err[y_err <= 0] = 1e-6
    return ((y - m) / y_err) ** 2

def gaussian(
    params: lmfit.Parameters,
    x: np.ndarray
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    s = params["s"].value
    B = params["B"].value
    return A * np.exp(-(x - f0)**2 / s**2) + B

class Gaussian(ModelBase):
    MODELSTR = r"A e^{((f - f_0) / s)^2} + B"
    PARAMS = {"A", "f0", "s", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return gaussian(params, x)

def double_gaussian(
    params: lmfit.Parameters,
    x: np.ndarray
) -> np.ndarray:
    A1 = params["A1"].value
    f1 = params["f1"].value
    s1 = params["s1"].value
    # B1 = params["B1"].value
    A2 = params["A2"].value
    f2 = params["f2"].value
    s2 = params["s2"].value
    B = params["B"].value
    return A1 * np.exp(-(x - f1)**2 / s1**2) + B + A2 * np.exp(-(x - f2)**2 / s2**2)

class DoubleGaussian(ModelBase):
    MODELSTR = r"A_1 e^{((f - f_1) / s_1)^2} + A_2 e^{((f - f_2) / s_2)^2} + B"
    PARAMS = {"A1", "f1", "s1", "A2", "f2", "s2", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return double_gaussian(params, x)

def lorentzian(
    params: lmfit.Parameters,
    x: np.ndarray
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    s = params["s"].value
    B = params["B"].value
    return A / (1 + (x - f0)**2 / s**2) + B

class Lorentzian(ModelBase):
    MODELSTR = r"\frac{A}{1 + ((f - f_0) / s)^2} + B"
    PARAMS = {"A", "f0", "s", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return lorentzian(params, x)

def lorentzian_multi(
    params: lmfit.Parameters,
    x: np.ndarray
) -> np.ndarray:
    n = int(params["n"].value)
    A = get_paramvals(params, *[f"A{k}" for k in range(n)])
    f = get_paramvals(params, *[f"f{k}" for k in range(n)])
    s = get_paramvals(params, *[f"s{k}" for k in range(n)])
    B = params["B"].value
    return sum(
        Ak / (1 + (x - fk)**2 / sk**2)
        for Ak, fk, sk in zip(A, f, s)
    ) + B

class LorentzianMulti(ModelBase):
    MODELSTR = r"B + \sum_n \frac{A_n}{1 + ((f - f_n) / s_n)^2}"
    PARAMS = set()

    def __init__(self, n: int):
        self.n = abs(n)
        self.PARAMS = {
            *{f"A{k}" for k in range(self.n)},
            *{f"f{k}" for k in range(self.n)},
            *{f"s{k}" for k in range(self.n)},
            "B"
        }

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        A = get_paramvals(params, *[f"A{k}" for k in range(self.n)])
        f = get_paramvals(params, *[f"f{k}" for k in range(self.n)])
        s = get_paramvals(params, *[f"s{k}" for k in range(self.n)])
        B = params["B"].value
        return sum(
            Ak / (1 + ((x - fk) / sk)**2)
            for (Ak, fk, sk) in zip(A, f, s)
        ) + B

def autlertowns(
    params: lmfit.Parameters,
    x: np.ndarray
) -> np.ndarray:
    f0 = params["f0"].value
    df = params["df"].value
    Al = params["Al"].value
    Ar = params["Ar"].value
    sl = params["sl"].value
    sr = params["sr"].value
    B = params["B"].value
    fl = f0 - df
    fr = f0 + df
    return Al/(1+((x-fl)/sl)**2) + Ar/(1+((x-fr)/sr)**2) + B

class AutlerTowns(ModelBase):
    MODELSTR = (
        r"\frac{A_l}{1 + ((f - f_l) / s_l)^2}"
        r" + \frac{A_r}{1 + ((f - f_r) / s_r)^2}"
    )
    PARAMS = {"Al", "Ar", "f0", "df", "sl", "sr", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return autlertowns(params, x)

def motional_sideband(
    params: lmfit.Parameters,
    x:np.ndarray
) -> np.ndarray:
    f0 = params["f0"].value
    A0 = params["A0"].value
    s0 = params["s0"].value
    df = params["df"].value
    Al = params["Al"].value
    Ar = params["Ar"].value
    s = params["s"].value
    B = params["B"].value
    return A0 / (1 + (x - f0)**2 / s0**2) \
        + Al / (1 + (x - (f0 - df)**2 / s**2)) \
        + Ar / (1 + (x - (f0 + df)**2 / s**2)) \
        + B

class MotionalSideband(ModelBase):
    MODELSTR = (
        r"\frac{A_l}{1 + ((f - df) / s)^2}"
        r"\frac{A0}{1 + ((f - f_0) / s_0)^2}"
        r" + \frac{A_r}{1 + ((f + df) / s)^2}"
    )
    PARAMS = {"f0", "A0", "s0", "df", "Al", "Ar", "s", "B",}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return motional_sideband(params, x)

def lightshift(
    params: lmfit.Parameter,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    return -A * (1 / (f0-x) + 1 / (f0+x))

class LightShift(ModelBase):
    MODELSTR = r"A \left( \frac{1}{f_0 - f} + \frac{1}{f_0 + f} \right)"
    PARAMS = {"A", "f0"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return lightshift(params, x)

def lightshift_offs(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    B = params["B"].value
    return -A * (1 / (f0 - x) + 1 / (f0 + x)) + B

class LightShiftOffs(ModelBase):
    MODELSTR = r"A \left( \frac{1}{f_0 - f} + \frac{1}{f_0 + f} \right) + B"
    PARAMS = {"A", "f0", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return lightshift_offs(params, x)

def exponential(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    tau = params["tau"].value
    B = params["B"].value
    return B - A * np.exp(-x / tau)

class DecayingExponential(ModelBase):
    MODELSTR = r"A e^{-t / \tau} + B"
    PARAMS = {"A", "tau", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        [A, tau, B] = get_paramvals(params, "A", "tau", "B")
        return A * np.exp(-x / tau) + B

class SaturatingExponential(ModelBase):
    MODELSTR = r"B - A e^{-t / \tau}"
    PARAMS = {"A", "tau", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        [A, tau, B] = get_paramvals(params, "A", "tau", "B")
        return B - A * np.exp(-x / tau)

def exponential_hump(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    tau0 = params["tau0"].value
    tau1 = params["tau1"].value
    B = params["B"].value
    return A * (1 - np.exp(-x / tau0)) * np.exp(-x / tau1) + B

class ExponentialHump(ModelBase):
    MODELSTR = r"A (1 - e^{-x / \tau_0}) e^{-x / \tau_1}"
    PARAMS = {"A", "tau0", "tau1", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return exponential_hump(params, x)

def sinc(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    a = params["a"].value
    b = params["b"].value
    B = params["B"].value
    zero_lim = A * a / b + B
    with np.errstate(divide="ignore"):
        ret = A * np.sin(a * (x - f0)) / (b * (x - f0)) + B
    ret[np.where(x == f0)] = zero_lim
    return ret

class Sinc(ModelBase):
    MODELSTR = r"A \frac{\sin(a (x - f_0))}{b (x - f_0)} + B"
    PARAMS = {"A", "f0", "a", "b", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return sinc(params, x)

def sinc2(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    A = params["A"].value
    f0 = params["f0"].value
    a = params["a"].value
    b = params["b"].value
    B = params["B"].value
    zero_lim = A * (a / b)**2 + B
    with np.errstate(divide="ignore"):
        ret = A * (np.sin(a * (x - f0)) / (b * (x - f0)))**2 + B
    ret[np.where(x == f0)] = zero_lim
    return ret

class Sinc2(ModelBase):
    MODELSTR = r"A \left(\frac{\sin(a (x - f_0))}{b (x - f_0)}\right)^2 + B"
    PARAMS = {"A", "f0", "a", "b", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return sinc2(params, x)

def decaying_oscillation(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    [A, f, ph, gam, B] = get_paramvals(params, "A", "f", "ph", "gam", "B")
    return (
        A * np.exp(-2 * np.pi * gam * x)
            * np.cos(2 * np.pi * f * x + np.pi / 180.0 * ph)
        + B
    )

class DecayingOscillation(ModelBase):
    MODELSTR = r"A e^{-2 \pi \gamma t} \cos(2 \pi f t + (\pi / 180) \phi) + B"
    PARAMS = {"A", "f", "ph", "gam", "B"}

    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return decaying_oscillation(params, x)

def decaying_oscillation_with_offset(
    params: lmfit.Parameters,
    x: np.ndarray,
) -> np.ndarray:
    [A, f, ph, gam, B, b, a] = get_paramvals(params, "A", "f", "ph", "gam", "B", "b", "a")
    return (
        A * np.exp(-2 * np.pi * gam * x)
            * np.cos(2 * np.pi * f * x + np.pi / 180 * ph)
        + b * x
        + B * np.exp(-2 * np.pi * a * x)
    )    

class DecayingOscillationWithOffset(ModelBase):
    MODELSTR = r"A e^{-2 \pi \gamma t} \cos(2 \pi f t + (\pi / 180) \phi)" \
        + r"+ bx + B e^{-2 \pi ax}"
    PARAMS = {"A", "f", "ph", "gam", "B", "b", "a"}
    def f(self, params: lmfit.Parameters, x: np.ndarray) -> np.ndarray:
        return decaying_oscillation_with_offset(params,x)