from __future__ import annotations
import re
from typing import Optional
try:
    from typing import Self
except ImportError:
    from typing import TypeVar
    Self = TypeVar("Self")
import lmfit
import numpy as np

def opt_or(x, default):
    return x if x is not None else default

def value_str(
    x: float,
    err: float,
    trunc: bool=True,
    sign: bool=False,
    sci: bool=False,
    latex: bool=False,
    dec: Optional[int]=None,
) -> str:
    """
    Prints a value and its standard error.

    Parameters
    ----------
    x : float
        The value.
    err : float
        The standard error.
    trunc : bool
        Print using "truncated" notation, e.g. "x.y(z)". If false, print instead
        with a "+/-", e.g. "x.y +/- 0.z".
    sign : bool
        Include a "+" for positive values.
    sci : bool
        Print using scientific notation, e.g. "x.y(z)e2" instead of "xy0(z0)".
    latex : bool
        Surround the expression with "$", and make the following replacements:
            * "+/-" => "\\pm"
            * "eXY" => " \\times 10^{XY}"
    """
    ord_x = np.floor(np.log10(abs(x)))
    ord_err = (
        np.floor(np.log10(err)) if np.isfinite(err) and err > 1e-12 else None
    )
    if sci:
        xp = (
            round(x / 10**opt_or(ord_err, 0))
            * 10**(opt_or(ord_err, 0) - ord_x)
        )
        errp = (
            round(err / 10**ord_err) * 10**(ord_err - ord_x)
        ) if ord_err is not None else None
        z = max(ord_x - opt_or(ord_err, 0), 0)
    else:
        xp = (
            round(x / 10**opt_or(ord_err, 0))
            * 10**opt_or(ord_err, 0)
        )
        errp = (
            round(err / 10**ord_err) * 10**ord_err
        ) if ord_err is not None else None
        z = max(-opt_or(ord_err, 0), 0)
    z = abs(min(dec, z) if dec is not None else z)
    if trunc:
        outstr = "{}({}){}".format(
            f"{{:+.{z:.0f}f}}".format(xp)
                if sign else f"{{:.{z:.0f}f}}".format(xp),
            "{:.0f}".format(errp * 10**z) if errp is not None else "nan",
            (
                " \\times 10^{{{:.0f}}}".format(ord_x) if latex
                else "e{}{:02.0f}".format("-" if ord_x < 0 else "+", abs(ord_x))
            ) if sci else "",
        )
    else:
        outstr = "{}{ex} {} {}{ex}".format(
            f"{{:+.{z:.0f}f}}".format(xp)
                if sign else f"{{:.{z:.0f}f}}".format(xp),
            r"\pm" if latex else "+/-",
            f"{{:.{z:.0f}f}}".format(errp) if errp is not None else "nan",
            ex=(
                " \\times 10^{{{:.0f}}}".format(ord_x) if latex
                else "e{}{:03.0f}".format("-" if ord_x < 0 else "+", abs(ord_x))
            ) if sci else "",
        )
    if latex:
        return "$" + outstr + "$"
    else:
        return outstr

class ExpVal:
    """
    An experimental value, with associated standard error.

    Arithmetic operations on this value will propagate the standard error in the
    appropriate way.
    """

    val: float
    err: float

    @staticmethod
    def from_x(f: Self | float | int) -> Self:
        """
        Create a new `ExpVal` from a number or another `ExpVal`. If the value is
        an `ExpVal`, this operation is the identity. Otherwise, a new `ExpVal`
        is created with zero error.
        """
        number = (
            float, int,
            np.float16, np.float32, np.float64, np.float128,
            np.int8, np.int16, np.int32, np.int64,
        )
        if isinstance(f, number):
            return ExpVal(float(f), 0.0)
        elif isinstance(f, ExpVal):
            return f
        else:
            raise NotImplementedError

    @staticmethod
    def from_str(s: str) -> Self:
        """
        Parse a string as an `ExpVal`.
        """
        SIGN = r"[+\-]"
        NON_NORMAL = "nan|inf"
        EXP = r"e([+\-]?\d+)"
        DIGITS = "[0123456789]"
        number = r"{nn}|(({d}*\.)?{d}+)".format(nn=NON_NORMAL, d=DIGITS)
        trunc = r"(({s}?)({n}))\(({nn}|{d}+)\)({e})?".format(
            s=SIGN, n=number, nn=NON_NORMAL, d=DIGITS, e=EXP,
        )
        pm = r"(({s}?)({n}({e})?))[ ]*(\+[/]?-|\\pm)[ ]*({n}({e})?)".format(
            s=SIGN, n=number, e=EXP,
        )
        rgx = r"^([$])?({t}|{p})([$])?$".format(
            t=trunc, p=pm,
        )
        pat = re.compile(rgx)
        if (cap := pat.match(s.lower())) is not None:
            if bool(cap.group(1)) ^ bool(cap.group(24)):
                raise Exception("unmatched '$'")
            if "(" in cap.group(2):
                val = float(cap.group(3))
                val_str = cap.group(5).replace(".", "")
                z = (
                    0 if val_str == "nan" or val_str == "inf"
                    else len(val_str) - 1
                )
                err = float(cap.group(8))
                p = float(cap.group(10) if cap.group(10) is not None else "0")
                val = val * 10**p
                err = err * 10**(p - z)
            else:
                val = float(cap.group(11))
                err = float(cap.group(19))
        else:
            raise Exception("malformed input")
        return ExpVal(val, err)

    def as_list(self) -> list[float]:
        """
        Convert to a two-item list, where the first item is the value and the
        second is the error.
        """
        return [self.val, self.err]

    def as_bounds(self) -> list[float]:
        """
        Convert to a two-item list, where the first item is the lower bound of
        the value, and the second is the upper bound.
        """
        return [self.val - self.err, self.val + self.err]

    def __init__(self, val: float, err: float):
        self.val = float(val)
        self.err = abs(float(err)) if err is not None else np.nan

    def __eq__(self, rhs: Self | float | int) -> bool:
        other = ExpVal.from_num_or(rhs)
        return self.val == other.val

    def __ne__(self, rhs: Self | float | int) -> bool:
        return not (self == rhs)

    def __lt__(self, rhs: Self | float | int) -> bool:
        other = ExpVal.from_num_or(rhs)
        return self.val < other.val

    def __gt__(self, rhs: Self | float | int) -> bool:
        other = ExpVal.from_num_or(rhs)
        return self.val > other.val

    def __le__(self, rhs: Self | float | int) -> bool:
        return not (self > rhs)

    def __ge__(self, rhs: Self | float | int) -> bool:
        return not (self < rhs)

    def __pos__(self) -> Self:
        return self

    def __neg__(self) -> Self:
        return ExpVal(
            -self.val,
            self.err,
        )

    def __add__(self, rhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(rhs)
        return ExpVal(
            self.val + other.val,
            np.sqrt(self.err**2 + other.err**2),
        )

    def __radd__(self, lhs: Self | float | int) -> Self:
        return self + lhs

    def __iadd__(self, rhs: Self | float | int):
        other = ExpVal.from_num_or(rhs)
        self.val += other.val
        self.err = np.sqrt(self.err**2 + other.err**2)

    def __sub__(self, rhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(rhs)
        return ExpVal(
            self.val - other.val,
            np.sqrt(self.err**2 + other.err**2),
        )

    def __rsub__(self, lhs: Self | float | int):
        other = ExpVal.from_num_or(lhs)
        return ExpVal(
            other.val - self.val,
            np.sqrt(other.err**2 + self.err**2),
        )

    def __isub__(self, rhs: Self | float | int):
        other = ExpVal.from_num_or(rhs)
        self.val -= other.val
        self.err = np.sqrt(self.err**2 + other.err**2)

    def __mul__(self, rhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(rhs)
        return ExpVal(
            self.val * other.val,
            np.sqrt((self.err * other.val)**2 + (self.val * other.err)**2),
        )

    def __rmul__(self, lhs: Self | float | int) -> Self:
        return self * lhs

    def __imul__(self, rhs: Self | float | int):
        other = ExpVal.from_num_or(rhs)
        self.val *= other.val
        self.err = np.sqrt((self.err * other.val)**2 + (self.val * other.err)**2)

    def __truediv__(self, rhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(rhs)
        return ExpVal(
            self.val / other.val,
            np.sqrt(
                (self.err / other.val)**2
                + (other.err * self.val / other.val**2)**2
            ),
        )

    def __rtruediv__(self, lhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(lhs)
        return ExpVal(
            other.val / self.val,
            np.sqrt(
                (other.err / self.val)**2
                + (self.err * other.val / self.val**2)**2
            ),
        )

    def __itruediv__(self, rhs: Self | float | int):
        other = ExpVal.from_num_or(rhs)
        self.val /= other.val
        self.err = np.sqrt(
            (self.err / other.val)**2
            + (other.err * self.val / other.val**2)**2
        )

    def __mod__(self, rhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(rhs)
        return ExpVal(
            self.val % other.val,
            np.sqrt(
                self.err**2
                + (np.floor(self.val / other.val) * other.err)**2
            ),
        )

    def __rmod__(self, lhs: Self | float | int) -> Self:
        other = ExpVal.from_num_or(lhs)
        return ExpVal(
            other.val % self.val,
            np.sqrt(
                other.err**2
                + (np.floor(other.val / self.val) * self.err)**2
            ),
        )

    def __imod__(self, rhs: Self | float | int):
        other = ExpVal.from_num_or(rhs)
        self.val %= other.val
        self.err = np.sqrt(
            self.err**2
            + (np.floor(self.val / other.val) * other.err)**2
        )

    def abs(self) -> Self:
        return ExpVal(
            abs(self.val),
            self.err,
        )

    def __abs__(self) -> Self:
        return self.abs()

    def abs_sub(self, other: Self | float | int) -> Self:
        other = ExpVal.from_num_or(other)
        return ExpVal(
            abs(self.val - other.val),
            np.sqrt(self.err**2 + other.err**2),
        )

    def acos(self) -> Self:
        return ExpVal(
            np.arccos(self.val),
            self.err / np.sqrt(1 - self.val**2),
        )

    def acosh(self) -> Self:
        return ExpVal(
            np.arccosh(self.val),
            self.err / np.sqrt(self.val**2 - 1),
        )

    def asin(self) -> Self:
        return ExpVal(
            np.arcsin(self.val),
            self.err / np.sqrt(1 - self.val**2),
        )

    def asinh(self) -> Self:
        return ExpVal(
            np.arcsinh(self.val),
            self.err / np.sqrt(self.val**2 + 1),
        )

    def atan(self) -> Self:
        return ExpVal(
            np.arctan(self.val),
            self.err / (self.val**2 + 1),
        )

    def atan2(self, other: Self | float | int) -> Self:
        other = ExpVal.from_num_or(other)
        return ExpVal(
            np.arctan2(self.val, other.val),
            np.sqrt(
                (self.val * other.err)**2
                + (self.err * other.val)**2
            ) / (self.val**2 + other.val**2),
        )

    def atanh(self) -> Self:
        return ExpVal(
            np.arctanh(self.val),
            self.err / abs(self.val**2 - 1),
        )

    def cbrt(self) -> Self:
        return ExpVal(
            pow(self.val, 1 / 3),
            self.err / pow(self.val, 2 / 3) / 3,
        )

    def ceil(self) -> Self:
        return ExpVal(
            np.ceil(self.val),
            0.0,
        )

    def __ceil__(self) -> Self:
        return self.ceil()

    def is_finite(self) -> bool:
        return np.isfinite(self.val)

    def is_inf(self) -> bool:
        return np.isinf(self.val)

    def is_nan(self) -> bool:
        return np.isnan(self.val)

    def is_neginf(self) -> bool:
        return np.isneginf(self.val)

    def is_posinf(self) -> bool:
        return np.isposinf(self.val)

    def cos(self) -> Self:
        return ExpVal(
            np.cos(self.val),
            self.err * abs(np.sin(self.val)),
        )

    def cosh(self) -> Self:
        return ExpVal(
            np.cosh(self.val),
            self.err * abs(np.sinh(self.val)),
        )

    def exp(self) -> Self:
        ex = np.exp(self.val)
        return ExpVal(
            ex,
            self.err * ex,
        )

    def exp2(self) -> Self:
        ex2 = np.exp2(self.val)
        return ExpVal(
            ex2,
            self.err * np.log(2) * ex2,
        )

    def exp_m1(self) -> Self:
        return ExpVal(
            np.expm1(self.val),
            self.err * np.exp(self.val),
        )

    def floor(self) -> Self:
        return ExpVal(
            np.floor(self.val),
            0.0,
        )

    def __floor__(self) -> Self:
        return self.floor()

    def frac(self) -> Self:
        return self % 1

    def hypot(self, other: Self | float | int) -> Self:
        other = ExpVal.from_num_or(other)
        h = np.sqrt(self.val**2 + other.val**2)
        return ExpVal(
            h,
            np.sqrt(
                (self.err * self.val)**2
                + (other.err * other.val)**2
            ) / h,
        )

    @staticmethod
    def infinity() -> Self:
        return ExpVal(
            np.inf,
            np.nan,
        )

    def ln(self) -> Self:
        return ExpVal(
            np.log(self.val),
            self.err / abs(self.val),
        )

    def ln_1p(self) -> Self:
        return ExpVal(
            np.log1p(self.val),
            self.err / abs(self.val + 1),
        )

    def log(self, base: Self | float | int) -> Self:
        base = ExpVal.from_num_or(base)
        return ExpVal(
            np.log(self.val) / np.log(base.val),
            np.sqrt(
                (
                    (self.err / self.val)**2
                    + (
                        base.err
                        * np.log(self.val)
                        / np.log(base.val)
                        / base.val
                    )**2
                ) / np.log(base.val)
            ),
        )

    def log10(self) -> Self:
        return ExpVal(
            np.log10(self.val),
            self.err / np.log(10) / abs(self.val),
        )

    def log2(self) -> Self:
        return ExpVal(
            np.log2(self.val),
            self.err / np.log(2) / abs(self.val),
        )

    def max(self, other: Self | float | int) -> Self:
        other = ExpVal.from_num_or(other)
        if self > other:
            return self
        elif self == other:
            return self
        elif self < other:
            return other
        else:
            raise Exception

    def min(self, other: Self | float | int) -> Self:
        other = ExpVal.from_num_or(other)
        if self > other:
            return other
        elif self == other:
            return self
        elif self < other:
            return self
        else:
            raise Exception

    @staticmethod
    def nan() -> Self:
        return ExpVal(
            np.nan,
            np.nan,
        )

    @staticmethod
    def neg_infinity() -> Self:
        return ExpVal(
            -np.inf,
            np.nan,
        )

    def pow(self, n: Self | float | int) -> Self:
        n = ExpVal.from_num_or(n)
        return ExpVal(
            pow(self.val, n.val),
            pow(self.val, n.val - 1) * np.sqrt(
                (self.err * n.val)**2
                + (n.err * self.val * np.log(self.val))**2
            ),
        )

    def recip(self) -> Self:
        return ExpVal(
            1 / self.val,
            self.err / self.val**2,
        )

    def round(self) -> Self:
        return ExpVal(
            np.round(self.val),
            0.0,
        )

    def __round__(self) -> Self:
        return self.round()

    def signum(self) -> Self:
        return ExpVal(
            -1.0 if self.val < 0.0 else +1.0,
            0.0,
        )

    def sin(self) -> Self:
        return ExpVal(
            np.sin(self.val),
            self.err * abs(np.cos(self.val)),
        )

    def sin_cos(self) -> (Self, Self):
        return (self.sin(), self.cos())

    def sinh(self) -> Self:
        return ExpVal(
            np.sinh(self.val),
            self.err * np.cosh(self.val),
        )

    def sqrt(self) -> Self:
        sq = np.sqrt(self.val)
        return ExpVal(
            sq,
            self.err / sq / 2,
        )

    def tan(self) -> Self:
        return ExpVal(
            np.tan(self.val),
            self.err / np.cos(self.val)**2,
        )

    def tanh(self) -> Self:
        return ExpVal(
            np.tanh(self.val),
            self.err / np.cosh(self.val)**2,
        )

    def trunc(self) -> Self:
        return ExpVal(
            np.trunc(self.val),
            0.0,
        )

    def __repr__(self) -> str:
        return f"ExpVal(val: {self.val:g}, err: {self.err:g})"

    def __str__(self) -> str:
        return value_str(self.val, self.err)

    def value_str(
        self,
        trunc: bool=True,
        sign: bool=False,
        sci: bool=False,
        latex: bool=False,
        dec: Optional[int]=None,
    ) -> str:
        """
        Thin alias for a call to `value_str`.
        """
        return value_str(self.val, self.err, trunc, sign, sci, latex, dec)

