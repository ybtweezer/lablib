import numpy as np
from functools import wraps
from typing import Optional

Real = (int, np.int32, np.int64, float, np.float32, np.float64)
NPReal = (*Real, np.ndarray)
Int = (int, np.int32, np.int64)
NPInt = (*Int, np.ndarray)
List_like = (list, tuple)
NPList_like = (*List_like, np.ndarray)
Iterable = (*List_like, set)
NPIterable = (*Iterable, np.ndarray)
Parameter = (*Real, type(None))
Function = type(lambda:0)

class BadType(Exception):
    def __init__(self, argname, given=None, expected=None):
        if given != None and expected != None:
            self.message = f"arg `{argname}` is not of expected type (expected {expected}, but got {given})"
        elif expected == None:
            self.message = f"arg `{argname}` is not of expected type (expected {expected})"
        elif given == None:
            self.message = f"arg `{argname}` is not of expected type (got {given})"
        else:
            self.message = f"arg `{argname}` is not of expected type"
        Exception.__init__(self, self.message)

def typecheck(types, conds=dict()):
    """
    Decorator which performs a high-level check that all arguments passed to a
    given function satisfy specified type requirements and additional conditions
    expressable as single-argument functions which return `True` or `False`.

    Parameters
    ----------
    types : dict-like[str : type or Tuple of types or None]
        Dict-like mapping argument names (as strs) to types or Tuples of types
        or `None`. If an argument name is mapped to `None`, then all types are
        accepted, and additional conditions on the argument are still performed.
    conds : dict-like[int : True/False Callable or Iterable of True/False Callables]
        Dict-like mapping argument names (as strs) to single-argument Callables
        which return `True` or `False` (with `True` being returned when a
        favorable condition is satisfied) or an Iterable of such Callables.
    """
    def decorator(f):
        @wraps(f)
        def checker(*args, **kwargs):
            if do_typecheck:
                argnames = f.__code__.co_varnames
                argvs = dict()
                argvs.update(kwargs)
                argvs.update(dict(zip(argnames, args)))

                for arg in argvs.keys():
                    typespec = types.get(arg, None)
                    if typespec == None:
                        pass
                    elif not isinstance(argvs[arg], typespec):
                        if isinstance(typespec, tuple):
                            expected = [t.__name__ for t in typespec]
                        else:
                            expected = typespec.__name__
                        raise BadType(arg, type(argvs[arg]).__name__, expected)

                    conditions = conds.get(arg, list())
                    if isinstance(conditions, type(lambda:None)):
                        conditions = [conditions]
                    elif not isinstance(conditions, (tuple, set, list)):
                        raise Exception(
                            f"{f.__name__}: typecheck: invalid conditions"
                            f" provided for arg `{arg}`"
                        )
                    for cond in conditions:
                        try:
                            cond_check = cond(argvs[arg])
                        except Exception as e:
                            raise Exception(
                                f"{f.__name__}: typecheck: error"
                                f" occurred while testing conditions for arg"
                                f" {arg}:\n{e}"
                            )
                        if not cond_check:
                            raise Exception(
                                f"{f.__name__}: typecheck: arg"
                                f" `{arg}` did not meet specified conditions"
                            )
            return f(*args, **kwargs)
        return checker
    return decorator

class Q:
    """
    Simple tool for print debugging.

    This class defines three operations, each of which calls `print` on values
    passed to it, and then re-returns the values.
    ```
    > qq = Q()
    > qq(0)          # prints `0`
    > qq(0, "a")     # prints `[0, 'a']`
    > qq(x=0)        # prints `x: 0`
    > qq(x=0, y="a") # prints `{'x': 0, 'y': 'a'}`
    """
    def __call__(self, *args, **kwargs):
        """
        Prints and returns all args and keyword args passed to it.
        """
        if len(args) == 0 and len(kwargs) == 0:
            return
        elif len(args) == 1 and len(kwargs) == 0:
            print(args[0])
            return args[0]
        elif len(args) == 0 and len(kwargs) == 1:
            (key, val) = next(iter(kwargs.items()))
            print(f"{key}: {val}")
            return val
        else:
            if len(args) > 0:
                print(args)
            if len(kwargs) > 0:
                print(kwargs)
            return args if len(kwargs) == 0 else (args, kwargs)

    def __truediv__(self, X):
        """
        Prints and returns the object immediately following the `/`.
        """
        print(X)
        return X

    def __or__(self, X):
        """
        Prints and returns the result of evaluating everything following the
        `|`.
        """
        print(X)
        return X
qq = Q()

def print_write(outfile, s, end="\n", flush=True) -> None:
    """
    Print a string and write it to an output stream in one function call.
    """
    print(s, end=end, flush=flush)
    outfile.write(s+end)
    if flush:
        outfile.flush()
    return None

def gen_table_fmt(
    label_fmts: list[tuple[str, str, Optional[dict[str, int]]]],
    s: str="  ",
    L: int=12,
    P: int=5,
    K: int=2,
) -> (str, str):
    """
    Generate the column labels and format string of a table from a list of
    formating specifications.

    Parameters
    ----------
    label_fmts : list[tuple[str, str, Optional[dict[str, int]]]]
        List of 2- or 3-tuples specifying the formats of each table column. The
        first item is the column label as a string, the second is the string
        formatter code, one of {'s','s>','i','f','g','e'}, and the third item is
        a dictionary allowing for overriding values of `L` and `P` below, via
        the keys 'L' and 'P' respectively. This last item is optional and may be
        omitted.
        (
            'column label',
            x in {'s','s>','i','f','g','e'},
            {'L': length override, 'P': precision override} (optional)
        )
    L : int (optional)
        Specify the minimum string width of each column, to the nearest multiple
        of `K`. Defaults to 12.
    P : int (optional)
        Specify the number of decimal places to print for floating-point values.
        Defaults to 5.
    K : int (optional)
        Specify the unit column width.

    Returns
    -------
    head : str
        Header for the table.
    fmt : str
        Row format string with places for each column.
    """
    head = ""
    lines = ""
    fmt = ""
    names = list()
    for label_fmt in label_fmts:
        names.append(label_fmt[0])
        overrides = dict() if len(label_fmt) < 3 else label_fmt[2]
        l = overrides.get(
            "L",
            max(
                int((len(label_fmt[0]) + K - 1) / K) * K,
                L * (label_fmt[1] in ['e','f','g']),
            )
        )
        p = overrides.get("P", l - 7 if (l - 7 >= 1 and l - 7 <= P) else P)
        head += "{:"+str(l)+"s}"+s
        lines += l*"-" + s
        if label_fmt[1] == 's':
            fmt += "{:"+str(l)+"s}"+s
        elif label_fmt[1] == 's>':
            fmt += "{:>"+str(l)+"s}"+s
        elif label_fmt[1] == 'i':
            fmt += "{:"+str(l)+".0f}"+s
        elif label_fmt[1] in ['e', 'f', 'g']:
            fmt += "{:"+str(l)+"."+str(p)+label_fmt[1]+"}"+s
        else:
            raise Exception("Format is not one of {'s', 's>', 'i', 'f', 'g', 'e'}")
    head = head[:-len(s)]
    lines = lines[:-len(s)]
    fmt = fmt[:-len(s)]
    return head.format(*names)+"\n"+lines, fmt

