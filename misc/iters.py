"""
Rust-style iterator adapters.

All classes in this module inherit from `IterBase`; use `Iter` as the main
entrypoint.

>>> result = (
>>>     # `Iter` is used to convert any native Python iterator into one that
>>>     # implements `IterBase`
>>>     Iter(range(1000))
>>>     .filter(lambda n: n % 2 == 0)    # select only evens
>>>     .skip_while(lambda n: n < 100)   # discard all less than 100
>>>     .take(10)                        # take the first 10 of the remainder
>>>     .map(lambda n: n * 3)            # multiply each by 3
>>>     .fold(0, lambda acc, x: acc + x) # sum the elements
>>> )
>>> print(result)
> 3270
"""

from copy import copy
from itertools import combinations, permutations, product
from typing import Callable, Iterable, Iterator, Optional, Self

class IterBase[T]:
    """
    Abstract class definition for all iterator types. As in Rust, the state of
    the iterator is controlled via the `next` method; this method either returns
    the next element of the iterator, signalling advancement, or `None`,
    signalling that iteration has halted. This is a direct remapping of the
    usual Pythonic interface, where calling the built-in `next` function on the
    iterator (or, equivalently, the `__next__` magic method) will either return
    the next value or raise `StopIteration`. This formulation makes iterator
    evaluation lazy: values are only computed when needed.

    This base class contains many default implementations of methods that
    interface to the various other iterator types in this module (e.g. `Map` and
    `Filter`); any custom iterator type need only implement the `next` method to
    opt into this system. Likewise, default implementations of the `__iter__`
    and `__next__` magic methods are also provided so that, given an
    implementation of `next`, any custom iterator can be used in native Python
    `for` loops as well.

    >>> ## example implementation of `range`
    >>>
    >>> from typing import Optional
    >>>
    >>> class RangeIter(IterBase[int]):
    >>>     start: int
    >>>     step: int
    >>>     end: Optional[int]
    >>>
    >>>     # iterators are lazy, so we can have `end` be optional
    >>>     def __init__(self, start: int, step: int, end: Optional[int]=None):
    >>>         self.start = start
    >>>         self.step = step
    >>>         self.end = end
    >>>
    >>>     def next(self) -> Optional[int]:
    >>>         if self.end is not None:
    >>>             if self.start < self.end:
    >>>                 item = self.start
    >>>                 self.start += self.step
    >>>                 return item
    >>>             else:
    >>>                 return None
    >>>         else:
    >>>             item = self.start
    >>>             self.start += self.step
    >>>             return item
    >>>
    >>> # `RangeIter` inherits `IterBase`, so we can use any of its other
    >>> # iterator adapter methods for free!
    >>> for n in RangeIter(start=0, step=2, end=9).map(lambda n: n ** 2):
    >>>     print(n)
    > 0
    > 4
    > 16
    > 36
    > 64
    """

    def next(self) -> Optional[T]:
        """
        Advances the iterator and returns the next value.

        Returns `None` when iteration is finished. Note that particular
        implementations may choose to resume iteration after an initial `None`
        is returned, such that subsequent calls may or may not eventually start
        returning values again.
        """
        raise NotImplementedError("missing Iterator.next implementation")

    def __iter__(self) -> Self:
        return self

    def __next__(self) -> T:
        if (n := self.next()) is not None:
            return n
        else:
            raise StopIteration()

    def collect_list(self) -> list[T]:
        """
        Evaluate all elements of the iterator and collect them into a list.

        Since `IterBase` implements `__iter__`, this is a thin alias for
        `list(self)`.
        """
        return list(self)

    def all(self, pred: Callable[[T], bool]) -> bool:
        """
        Return `True` if all elements of the iterator satisfy a predicate,
        `False` otherwise.
        """
        return all(pred, self)

    def all_equal(self) -> bool:
        """
        Return `True` if all elements in the iterator are equal to each other.
        The iterator element type must implement `__eq__`.
        """
        last = None
        for x in self:
            if last is None:
                last = x
                continue
            elif not (x == last):
                return False
        return True

    def all_equal_by(self, eq: Callable[[T, T], bool]) -> bool:
        """
        Like `all_equal`, but using a given comparison function.
        """
        last = None
        for x in self:
            if last is None:
                last = x
                continue
            elif not eq(last, x):
                return False
        return True

    def all_equal_value(self) -> T | tuple[T, T]:
        """
        Like `all_equal`, but returning the value that all elements are equal
        to. If not all elements are equal to each other, return an example pair
        of two non-equal elements. The iterator element type must implement
        `__eq__`.
        """
        last = None
        for x in self:
            if last is None:
                last = x
                continue
            elif not (x == last):
                return (last, x)
        return last

    def all_equal_value_by(self, eq: Callable[[T, T], bool]) -> T | tuple[T, T]:
        """
        Like `all_equal_value`, but using a given comparison function.
        """
        last = None
        for x in self:
            if last is None:
                last = x
                continue
            elif not eq(last, x):
                return (last, x)
        return last

    def all_unique(self) -> bool:
        """
        Return `True` if no two elements in the iterator are equal to each
        other. The iterator element type must implement `__eq__`.
        """
        seen = list()
        for x in self:
            if x in seen:
                return False
            else:
                seen.append(x)
        return True

    def all_unique_by(self, eq: Callable[[T, T], bool]) -> bool:
        """
        Like `all_unique`, but using a given comparison function.
        """
        seen = list()
        for x in self:
            if any(lambda y: eq(x, y), seen):
                return False
            else:
                seen.append(x)
        return True

    def any(self, pred: Callable[[T], bool]) -> bool:
        """
        Return `True` if any element of the iterator satisfies a predicate,
        `False` otherwise.
        """
        return any(pred, self)

    def cartesian_product[U](
        self,
        other: 'IterBase[U]'
    ) -> 'CartesianProduct[T, U]':
        """
        Creates an iterator over the cartesian product of two iterators.
        """
        return CartesianProduct(self, other)

    def chain(self, other: 'IterBase[T]') -> 'Chain[T]':
        """
        Concatenates two iterators of the same type into a single iterator,
        attaching `other` to the end of `self`.
        """
        return Chain(self, other)

    def chunks(self, chunksize: int, exact: bool=False) -> 'Chunks[T]':
        """
        Breaks an iterator into chunks of size `chunksize`, yielding them as
        lists. Pass `exact=True` to make each chunk exactly `chunksize` elements
        long, in which case the returned iterator may not visit the last few
        elements of `self`. Otherwise, the last chunk in the returned iterator
        may be less than `chunksize` elements long.
        """
        return Chunks(chunksize, self, exact=exact)

    def chunk_by(
        self,
        pred: Callable[[T], bool],
        incl: bool=False,
    ) -> 'ChunkBy[T]':
        """
        Break an iterator into chunks using a predicate that returns `True` to
        signal a chunk division. Pass `incl=True` to include the matching
        element at the end of the chunk.
        """
        return ChunkBy(pred, self, incl=incl)

    def combinations(self, size: int) -> 'Combinations[T]':
        """
        Return an iterator over all `size`-element combinations of the elements
        of `self`.
        """
        return Combinations(size, self)

    def count(self) -> int:
        """
        Count the number of elements in `self`. This always fully consumes the
        `self`.
        """
        count = 0
        for _ in self:
            count += 1
        return count

    def counts(self) -> dict[T, int]:
        """
        Return a dictionary containing the number of occurrences of each value.
        """
        acc = dict()
        for x in self:
            if x in acc:
                acc[x] += 1
            else:
                acc[x] = 1
        return acc

    def counts_by[U](self, func: Callable[[T], U]) -> dict[U, int]:
        """
        Like `counts`, but passing iterator elements through `func` first.

        This is equivalent to `.map(func).counts()`.
        """
        acc = dict()
        for x in self:
            y = func(x)
            if y in acc:
                acc[y] += 1
            else:
                acc[y] = 1

    def cycle(self) -> 'Cycle[T]':
        """
        Return an iterator that endlessly loops through the elements of `self`.
        """
        return Cycle(self)

    def duplicates(self) -> 'Duplicates[T]':
        """
        Return an iterator over only the elements of `self` that occur more than
        once.
        """
        return Duplicates(self)

    def duplicates_by(self, func: Callable[[T, T], bool]) -> 'DuplicatesBy[T]':
        """
        Like `Duplicates`, but using a given comparison function.
        """
        return DuplicatesBy(func, self)

    def enumerate(self) -> 'Enumerate[T]':
        """
        Convert each element `x` of the iterator to `(k, x)`, where `k` is an
        integer representing `x`'s position in `self`.
        """
        return Enumerate(self)

    def filter(self, pred: Callable[[T], bool]) -> 'Filter[T]':
        """
        Return an iterator discarding all elements of `self` not satisfying a
        given predicate.
        """
        return Filter(pred, self)

    def filter_map[U](
        self,
        func: Callable[[T], Optional[U]]
    ) -> 'FilterMap[T, U]':
        """
        Combination of `filter` and `map` using a function that returns a mapped
        value if a predicate is satisfied, otherwise `None`.
        """
        return FilterMap(func, self)

    def find(self, pred: Callable[[T], bool]) -> Optional[T]:
        """
        Return the first element in `self` that satisfies a predicate.
        """
        for x in self:
            if pred(x):
                return x
        return None

    def find_map[U](self, func: Callable[[T], Optional[U]]) -> Optional[U]:
        """
        Applies a function to each element in `self` and returns the first
        result that is not `None`.
        """
        for x in self:
            if (y := func(x)) is not None:
                return y
        return None

    def flat_map[U](
        self,
        func: Callable[[T], 'IterBase[U]']
    ) -> 'FlatMap[T, U]':
        """
        Creates an iterator that works like `map`, but flattens nested
        iterator structures. This is equivalent to `.map(func).flatten()`.
        """
        return FlatMap(func, self)

    def flatten(self) -> 'Flatten[T]':
        """
        Creates an iterator that flattens nested iterator structures. This
        operation is sometimes called `concat`.
        """
        return Flatten(self)

    def fold[U](self, init: U, func: Callable[[U, T], U]) -> U:
        """
        Perform an accumulating operation over the elements of `self`, returning
        the final result. Given an initial value `init`, this operation computes
            func( ... func(func(init, x0), x1), ...)
        where `xk` are the elements of `self`. This operation is sometimes
        called `reduce`.
        """
        acc = init
        for x in self:
            acc = func(acc, x)
        return acc

    def for_each(self, func: Callable[[T], None]):
        """
        Apply an operation to each element of `self`. This is equivalent to a
        `for` loop that calls `func` on each element of `self`.
        """
        for x in self:
            func(x)

    def inspect(self, func: Callable[[T], None]) -> 'Inspect[T]':
        """
        Return an iterator that calls `func` on each element of `self` and
        yields the original value. This operation can be useful for debugging
        purposes.
        """
        return Inspect(func, self)

    def interleave(self, other: 'IterBase[T]') -> 'Interleave[T]':
        """
        Return an iterator that alternates elements from `self` and `other`
        until either have run out.
        """
        return Interleave(self, other)

    def interleave_longest(
        self,
        other: 'IterBase[T]',
    ) -> 'InterleaveLongest[T]':
        """
        Like `interleave`, but yielding elements until *both* `self` and `other`
        have run out; i.e. if one is shorter than the other, the tail end of the
        returned iterator will comprise elements from only one source iterator.

        To alternate cyclically between more than two iterators, use the
        `InterleaveLongest` constructor directly.
        """
        return InterleaveLongest(self, other)

    def intersperse(self, elem: T) -> 'Intersperse[T]':
        """
        Return an iterator that inserts `elem` between each element of `self`.
        """
        return Intersperse(elem, self)

    def intersperse_with(self, gen: Callable[[], T]) -> 'IntersperseWith[T]':
        """
        Like `intersperse`, but using a generating function instead of a
        concrete value.
        """
        return IntersperseWith(gen, self)

    def k_largest(self, k: int) -> list[T]:
        """
        Return the `k` largest elements of `self` in ascending order.

        The iterator element type must implement comparison magic methods.
        """
        items = list(self)
        items.sort()
        return items[-k:]

    def k_largest_key[U](self, k: int, key: Callable[[T], U]) -> list[T]:
        """
        Like `k_largest`, but sorting on the output of a key function.

        The output type of the key function must implement comparison magic
        methods.
        """
        items = list(self)
        items.sort(key=key)
        return items[-k:]

    def k_smallest(self, k: int) -> list[T]:
        """
        Return the `k` smallest elements of `self` in ascending order.

        The iterator element type must implement comparison magic methods.
        """
        items = list(self)
        items.sort()
        return items[:k]

    def k_smallest_key[U](self, k: int, key: Callable[[T], U]) -> list[T]:
        """
        Like `k_smallest`, but sorting on the output of a key function.

        The output type of the key function must implement comparison magic
        methods.
        """
        items = list(self)
        items.sort(key=key)
        return items[:k]

    def last(self) -> Optional[T]:
        """
        Return the last element of `self`.

        Returns `None` if `self` is empty.
        """
        last = None
        for x in self:
            last = x
        return last

    def map[U](self, func: Callable[[T], U]) -> 'Map[T, U]':
        """
        Return an iterator over the output values of a function applied to each
        element in `self`.
        """
        return Map(func, self)

    def map_while[U](
        self,
        func: Callable[[T], Optional[U]],
    ) -> 'MapWhile[T, U]':
        """
        Like `map`, but only yield mapped values until the first `None` is
        encountered in the output.
        """
        return MapWhile(func, self)

    def max(self) -> Optional[T]:
        """
        Return the maximum element of `self`. If more than one element is
        equally maximal, return the first encountered. Returns `None` if `self`
        is empty.

        The iterator element type must implement `__gt__`.
        """
        acc = None
        for x in self:
            if acc is None:
                acc = x
            elif x > acc:
                acc = x
        return acc

    def max_by_key[U](self, key: Callable[[T], U]) -> Optional[tuple[T, U]]:
        """
        Like `max`, but using the output of a key function for comparison,
        returning both the source and output values through that function.
        """
        acc = None
        for x in self:
            if acc is None:
                acc = (x, key(x))
            elif (y := key(x)) > acc[1]:
                acc = (x, y)
        return acc

    def min(self) -> Optional[T]:
        """
        Return the minimum element of `self`. If more than one element is
        equally minimal, return the first encountered. Returns `None` if `self`
        is empty.

        The iterator element type must implement `__lt__`.
        """
        acc = None
        for x in self:
            if acc is None:
                acc = x
            elif x < acc:
                acc = x
        return acc

    def min_by_key[U](self, key: Callable[[T], U]) -> Optional[tuple[T, U]]:
        """
        Like `min`, but using the output of a key function for comparison,
        returning both the source and output values through that function.
        """
        acc = None
        for x in self:
            if acc is None:
                acc = (x, key(x))
            elif (y := key(x)) < acc[1]:
                acc = (x, y)
        return acc

    def multi_cartesian_product(self) -> 'MultiCartesianProduct[T]':
        """
        Like `cartesian_product`, but over the product of more than two
        subiterators. Only makes sense when `self` is an iterator of iterable
        elements.
        """
        return MultiCartesianProduct(*list(self))

    def multi_interleave(self) -> 'Interleave[T]':
        """
        Like `interleave`, but alternating cyclically between more than two
        subiterators. Only makes sense when `self` is an iterator of iterable
        elements.
        """
        return Interleave(*list(self))

    def multi_interleave_longest(self) -> 'InterleaveLongest[T]':
        """
        Like `interleave_longest`, but alternating cyclically between more than
        two subiterators. Only makes sense when `self` is an iterator of
        iterable elements.
        """
        return InterleaveLongest(*list(self))

    def peekable(self) -> 'Peekable[T]':
        """
        Convert `self` into an iterator that has an extra `peek` method that
        returns the next element in the iterator, but does not advance beyond
        that point. See `Peekable` for more information.
        """
        return Peekable(self)

    def permutations(self, size: Optional[int]=None) -> 'Permutations[T]':
        """
        Return an iterator over all `size`-element permutations of elements in
        `self`.
        """
        return Permutations(self, size=size)

    def position(self, pred: Callable[[T], bool]) -> Optional[tuple[int, T]]:
        """
        Return the position and value of the first element matching a predicate.
        This is equivalent to `.enumerate().find(lambda x: pred(x[1]))`
        """
        count = 0
        for x in self:
            if pred(x):
                return (count, x)
            count += 1
        return None

    def positions(self, pred: Callable[[T], bool]) -> 'Positions[T]':
        """
        Return an iterator over the positions of all elements matching a
        predicate. This is equivalent to
        `.enumerate().filter_map(lambda x: x[0] if pred(x[1]) else None)`.
        """
        return Positions(pred, self)

    def scan[S, U](
        self,
        init: S,
        func: Callable[[S, T], Optional[tuple[S, U]]]
    ) -> 'Scan[T, S, U]':
        """
        Like `fold`, but returning a new iterator over intermediate states. This
        method takes an initial state and a function `func` that computes a new
        state and a value, or returns `None` to end iteration early. If the
        result is not `None`, the new state is used in the computation of the 
        next item in the iterator. Only the computed value is yielded.
        """
        return Scan(init, func, self)

    def skip(self, n: int) -> 'Skip[T]':
        """
        Return an iterator that discards the first `n` elements of `self`.

        Note that these `n` elements *must still be computed* (which happens on
        the first call to `next` in the returned iterator); they only won't be
        yielded to the program.
        """
        return Skip(n, self)

    def skip_while(self, pred: Callable[[T], bool]) -> 'SkipWhile[T]':
        """
        Return an iterator that discards the first several elements that satisfy
        a predicate, returning the first that doesn't and all those remaining.
        """
        return SkipWhile(pred, self)

    def split_by(
        self,
        pred: Callable[[T], bool],
        incl: bool=False,
    ) -> 'SplitBy[T]':
        """
        Like `split_on`, but using a given predicate.
        """
        return SplitBy(pred, self, incl=incl)

    def split_on(
        self,
        elem: T,
        incl: bool=False,
    ) -> 'SplitOn[T]':
        """
        Split an iterator into groups based on the occurrance of a particular
        iterator element. When iteration encounters an instance equal to `elem`,
        a group is yielded in a list. Elements remaining in the iterator after
        the final split are also yielded. Pass `incl=True` to include the
        matched element at the end of each group.

        The iterator element type must implement `__eq__`.
        """
        return SplitOn(elem, self, incl=incl)

    def sorted(self) -> list[T]:
        """
        Return a list of all elements sorted in ascending order. This is a thin
        alias for `sorted(self)`.
        """
        return sorted(self)

    def sorted_key[U](self, key: Callable[[T], U]) -> list[T]:
        """
        Return a list of all elements sorted in ascending order according to a
        key function. This is a thin alias for `sorted(self, key=key)`.

        The output type of the key function must implement comparison magic
        methods.
        """
        return sorted(self, key=key)

    def step_by(self, stepsize: int) -> 'StepBy[T]':
        """
        Return an iterator that takes only every `stepsize`-th element of
        `self`. Note that *all discarded elements must still be computed*, and
        that the first element of the iterator will always be yielded,
        regardless of the step size.
        """
        return StepBy(stepsize, self)

    def take(self, n: int) -> 'Take[T]':
        """
        Return an iterator over only the first `n` elements of `self`.
        """
        return Take(n, self)

    def take_while(self, pred: Callable[[T], bool]) -> 'TakeWhile[T]':
        """
        Return an iterator over only the first several elements of `self` that
        satisfy a given predicate. Iteration is halted on the first occurrence
        of any element that doesn't satisfy the predicate.
        """
        return TakeWhile(pred, self)

    def unique(self) -> 'Unique[T]':
        """
        Return an iterator over only the elements of `self` that have not
        occurred before.

        The iterator element type must implement `__eq__`.
        """
        return Unique(self)

    def unique_by(self, eq: Callable[[T, T], bool]) -> 'UniqueBy[T]':
        """
        Like `unique`, but using a given comparison function.
        """
        return UniqueBy(eq, self)

    def windows(self, windowlen: int) -> 'Windows[T]':
        """
        Return an iterator over rolling "windows" of iterator elements as lists
        of length *at most* `windowlen`. At the head and tail ends of the
        returned iterator, yielded lists will in general have lengths less than
        `windowlen`.
        """
        return Windows(windowlen, self)

    def windows_exact(self, windowlen: int) -> 'WindowsExact[T]':
        """
        Like `windows`, but yielding windows of *exactly* `windowlen`.
        """
        return WindowsExact(windowlen, self)

    def zip[U](self, other: 'IterBase[U]') -> 'Zip[T, U]':
        """
        Return an iterator that iterates over `self` and `other` simultaneously
        in lock-step, yielding tuples of elements and ending whenever the
        shortest of the two does.
        """
        return Zip(self, other)

    def zip_longest[U](self, other: 'IterBase[U]') -> 'ZipLongest[T, U]':
        """
        Like `zip`, but ending only when both iterators run out. The yielded
        tuples may contain `None` in either position if either `self` or `other`
        is shorter than the other.
        """
        return ZipLongest(self, other)

class Iter[T](IterBase[T]):
    """
    Main entrypoint into the `IterBase` framework. `Iter` can be constructed
    from any Python iterable object.

    >>> it = Iter([0, 3, 1, 2])
    >>> assert it.next() == 0
    >>> assert it.next() == 3
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == None
    """
    source: Iterator[T]

    def __init__(self, source: Iterable[T]):
        self.source = iter(source)

    def next(self) -> Optional[T]:
        try:
            return next(self.source)
        except StopIteration:
            return None

class CartesianProduct[T, U](IterBase[tuple[T, U]]):
    """
    Iterate over the Cartesian product of two iterators.

    >>> it = Iter(range(2)).cartesian_product(Iter("ab"))
    >>> assert it.next() == (0, 'a')
    >>> assert it.next() == (0, 'b')
    >>> assert it.next() == (1, 'a')
    >>> assert it.next() == (1, 'b')
    >>> assert it.next() == None
    """
    source: Iter[tuple[T, U]]

    def __init__(self, source_l: IterBase[T], source_r: IterBase[U]):
        self.source = Iter(product(source_l, source_r))

    def next(self) -> Optional[tuple[T, U]]:
        return self.source.next()

class Chain[T](IterBase[T]):
    """
    Concatenate two iterators of the same type into a single iterator, attaching
    them head-to-tail in sequence.

    >>> it = Iter([0, 1]).chain(Iter([2, 3]))
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == None
    """
    source0: IterBase[T]
    source1: IterBase[T]

    def __init__(self, source0: IterBase[T], source1: IterBase[T]):
        self.source0 = source0
        self.source1 = source1

    def next(self) -> Optional[T]:
        if (n := self.source0.next()) is not None:
            return n
        elif (m := self.source1.next()) is not None:
            return m
        else:
            return None

class Chunks[T](IterBase[list[T]]):
    """
    Break an iterator into chunks of size `chunksize`, yielding them as lists.
    Pass `exact=True` to make each chunk exactly `chunksize` elements long, in
    which case the returned iterator may not visit the last few elements of
    the source iterator. Otherwise, the last chunk in the returned iterator may
    be less than `chunksize` elements long.

    >>> it = Iter(range(8)).chunks(3)
    >>> assert it.next() == [0, 1, 2]
    >>> assert it.next() == [3, 4, 5]
    >>> assert it.next() == [6, 7]
    >>> assert it.next() == None
    >>>
    >>> it = Iter(range(8)).chunks(3, exact=True)
    >>> assert it.next() == [0, 1, 2]
    >>> assert it.next() == [3, 4, 5]
    >>> assert it.next() == None
    """
    chunksize: int
    source: IterBase[T]
    chunk: list[T]
    exact: bool

    def __init__(self, chunksize: int, source: IterBase[T], exact: bool=False):
        self.chunksize = chunksize
        self.source = source
        self.chunk = list()
        self.exact = exact

    def next(self) -> Optional[list[T]]:
        while len(self.chunk) < self.chunksize:
            if (n := self.source.next()) is not None:
                self.chunk.append(n)
            elif len(self.chunk) > 0 and not self.exact:
                chunk = self.chunk
                self.chunk = list()
                return chunk
            else:
                return None
        if not self.exact:
            if len(self.chunk) > 0:
                chunk = self.chunk
                self.chunk = list()
                return chunk
            else:
                return None
        elif len(self.chunk) == self.chunksize:
            chunk = self.chunk
            self.chunk = list()
            return chunk

class ChunkBy[T](IterBase[list[T]]):
    """
    Break an iterator into chunks using a predicate that returns `True` to
    signal a chunk division. Pass `incl=True` to include the matching
    element at the end of the chunk.

    >>> it = Iter(range(12)).chunk_by(lambda n: n % 3 == 0)
    >>> assert it.next() == []
    >>> assert it.next() == [1, 2]
    >>> assert it.next() == [4, 5]
    >>> assert it.next() == [7, 8]
    >>> assert it.next() == None
    >>>
    >>> it = Iter(range(12)).chunk_by(lambda n: n % 3 == 0, incl=True)
    >>> assert it.next() == [0]
    >>> assert it.next() == [1, 2, 3]
    >>> assert it.next() == [4, 5, 6]
    >>> assert it.next() == [7, 8, 9]
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    incl: bool
    source: IterBase[T]
    chunk: list[T]

    def __init__(
        self,
        pred: Callable[[T], bool],
        source: IterBase[T],
        incl: bool=False,
    ):
        self.pred = pred
        self.source = source
        self.incl = incl
        self.chunk = list()

    def next(self) -> Optional[list[T]]:
        while (n := self.source.next()) is not None:
            if self.pred(n):
                if self.incl:
                    self.chunk.append(n)
                chunk = self.chunk
                self.chunk = list()
                return chunk
            else:
                self.chunk.append(n)
        return None

class Combinations[T](IterBase[tuple[T, ...]]):
    """
    Iterate over all fixed-size combinations of the elements of an iterator.

    >>> it = Iter(range(4)).combinations(2)
    >>> assert it.next() == (0, 1)
    >>> assert it.next() == (0, 2)
    >>> assert it.next() == (0, 3)
    >>> assert it.next() == (1, 2)
    >>> assert it.next() == (1, 3)
    >>> assert it.next() == (2, 3)
    >>> assert it.next() == None
    """
    source: Iter[tuple[T, ...]]

    def __init__(self, size: int, source: IterBase[T]):
        self.source = Iter(combinations(source, size))

    def next(self) -> Optional[tuple[T, ...]]:
        return self.source.next()

class Cycle[T](IterBase[T]):
    """
    Endlessly loop through the elements of an iterator.

    >>> it = Iter(range(4)).cycle()
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> # ...
    """
    source: IterBase[T]
    cache: list[T]
    pos: int

    def __init__(self, source: IterBase[T]):
        self.source = source
        self.cache = list()
        self.pos = 0

    def next(self) -> Optional[T]:
        if (n := self.source.next()) is not None:
            self.cache.append(n)
            return n
        elif (l := len(self.cache)) > 0:
            k = self.pos
            self.pos = (self.pos + 1) % l
            return self.cache[k]
        else:
            return None

class Duplicates[T](IterBase[T]):
    """
    Iterate over only the elements of an iterator that occur more than once.

    >>> it = Iter("aabcbbaede").duplicates()
    >>> assert it.next() == "a"
    >>> assert it.next() == "b"
    >>> assert it.next() == "b"
    >>> assert it.next() == "a"
    >>> assert it.next() == "e"
    >>> assert it.next() == None
    """
    source: IterBase[T]
    seen: list[T]

    def __init__(self, source: IterBase[T]):
        self.source = source
        self.seen = list()

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if n in self.seen:
                return n
            else:
                self.seen.append(n)
                continue
        return None

class DuplicatesBy[T](IterBase[T]):
    """
    Iterate over only the elements of an iterator that occur more than once
    according to a comparison function.

    >>> it = (
    >>>     Iter("abcde")
    >>>     .zip(Iter([0, 0, 1, 2, 1]))
    >>>     .duplicates_by(lambda x, y: x[1] == y[1])
    >>> )
    >>> assert it.next() == ("b", 0)
    >>> assert it.next() == ("e", 1)
    >>> assert it.next() == None
    """
    func: Callable[[T, T], bool]
    source: IterBase[T]
    seen: list[T]

    def __init__(self, func: Callable[[T, T], bool], source: IterBase[T]):
        self.func = func
        self.source = source
        self.seen = list()

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if any(self.func(n, x) for x in self.seen):
                return n
            else:
                self.seen.append(n)
                continue
        return None

class Enumerate[T](IterBase[tuple[int, T]]):
    """
    Convert each element `x` of the iterator to `(k, x)`, where `k` is an
    integer giving a position in the source iterator.

    >>> it = Iter("abc").enumerate()
    >>> assert it.next() == (0, "a")
    >>> assert it.next() == (1, "b")
    >>> assert it.next() == (2, "c")
    >>> assert it.next() == None
    """
    source: IterBase[T]
    count: int

    def __init__(self, source: IterBase[T]):
        self.source = source
        self.count = 0

    def next(self) -> Optional[tuple[int, T]]:
        if (n := self.source.next()) is not None:
            k = self.count
            self.count += 1
            return (k, n)
        else:
            return None

class Filter[T](IterBase[T]):
    """
    Discard all elements of an iterator not satisfying a given predicate.

    >>> it = Iter(range(10)).filter(lambda n: n % 2 == 0)
    >>> assert it.next() == 0
    >>> assert it.next() == 2
    >>> assert it.next() == 4
    >>> assert it.next() == 6
    >>> assert it.next() == 8
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    source: IterBase[T]

    def __init__(self, pred: Callable[[T], bool], source: IterBase[T]):
        self.pred = pred
        self.source = source

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if self.pred(n):
                return n
            else:
                continue
        return None

class FilterMap[T, U](IterBase[U]):
    """
    Combination of `Filter` and `Map`, keeping mapped values and discarding
    those for which the given function returns `None`.

    >>> it = Iter(range(10)).filter(lambda n: n ** 2 if n % 2 == 0 else None)
    >>> assert it.next() == 0
    >>> assert it.next() == 4
    >>> assert it.next() == 16
    >>> assert it.next() == 36
    >>> assert it.next() == 64
    >>> assert it.next() == None
    """
    func: Callable[[T], Optional[U]]
    source: IterBase[T]

    def __init__(self, func: Callable[[T], Optional[U]], source: IterBase[T]):
        self.func = func
        self.source = source

    def next(self) -> Optional[U]:
        if (n := self.source.next()) is not None:
            if (m := self.func(n)) is not None:
                return m
            else:
                return self.next()
        else:
            return None

class FlatMap[T, U](IterBase[U]):
    """
    Like `Map`, but flattens nested iterator structures. Only makes sense for
    mapping functions that return subiterators, which must be a subclass of
    `IterBase`.

    >>> it = Iter(range(4)).flat_map(lambda n: Iter((n + 1) * chr(97 + n)))
    >>> assert it.next() == "a"
    >>> assert it.next() == "b"
    >>> assert it.next() == "b"
    >>> assert it.next() == "c"
    >>> assert it.next() == "c"
    >>> assert it.next() == "c"
    >>> assert it.next() == "d"
    >>> assert it.next() == "d"
    >>> assert it.next() == "d"
    >>> assert it.next() == "d"
    >>> assert it.next() == None
    """
    func: Callable[[T], IterBase[U]]
    source: IterBase[T]
    group = Optional[IterBase[U]]

    def __init__(self, func: Callable[[T], IterBase[U]], source: IterBase[T]):
        self.func = func
        self.source = source
        self.group = None

    def next(self) -> Optional[U]:
        if self.group is not None:
            if (m := self.group.next()) is not None:
                return m
            elif (n := self.source.next()) is not None:
                self.group = self.func(n)
                return self.next()
            else:
                return None
        elif (n := self.source.next()) is not None:
            self.group = self.func(n)
            return self.next()
        else:
            return None

class Flatten[T](IterBase[T]):
    """
    Flatten nested iterable structures. Only makes sense when the iterator
    element type is a subclass of `IterBase`.

    >>> it = Iter([[0], [1, 2, 3], [4, 5], []]).flatten()
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 4
    >>> assert it.next() == 5
    """
    source: IterBase[Iterable[T]]
    group = Optional[Iter[T]]

    def __init__(self, source: IterBase[Iterable[T]]):
        self.source = source
        self.group = None

    def next(self) -> Optional[T]:
        if self.group is not None:
            if (m := self.group.next()) is not None:
                return m
            elif (g := self.source.next()) is not None:
                self.group = Iter(g)
                return self.next()
            else:
                return None
        elif (g := self.source.next()) is not None:
            self.group = Iter(g)
            return self.next()
        else:
            return None

class Inspect[T](IterBase[T]):
    """
    Call a function on each element of the iterator and yield the original
    element. Useful for debugging.

    >>> it = Iter(range(5)).inspect(print)
    >>> assert it.next() == 0 # prints 0
    >>> assert it.next() == 1 # prints 1 
    >>> assert it.next() == 2 # prints 2
    >>> assert it.next() == 3 # prints 3
    >>> assert it.next() == 4 # prints 4
    >>> assert it.next() == None
    """
    func: Callable[[T], None]
    source: IterBase[T]

    def __init__(self, func: Callable[[T], None], source: IterBase[T]):
        self.func = func
        self.source = source

    def next(self) -> Optional[T]:
        if (n := self.source.next()) is not None:
            self.func(n)
            return n
        else:
            return None

class Interleave[T](IterBase[T]):
    """
    Cyclically rotate through one or more iterators until any have run out.

    >>> it = Interleave(Iter([0, 3]), Iter([1, 4, 7, 10]), Iter([2, 5]))
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 4
    >>> assert it.next() == 5
    >>> assert it.next() == None
    """
    sources: list[IterBase[T]]
    yielding: bool
    pos: int
    n: int

    def __init__(self, *sources: IterBase[T]):
        self.sources = sources
        self.yielding = True
        self.pos = 0
        self.n = len(self.sources)

    def next(self) -> Optional[T]:
        if self.yielding:
            if (n := self.sources[self.pos].next()) is not None:
                self.pos = (self.pos + 1) % self.n
                return n
            else:
                self.yielding = False
                return None
        else:
            return None

class InterleaveLongest[T](IterBase[T]):
    """
    Like `Interleave`, but continuing until all iterators have run out.

    >>> it = Interleave(Iter([0, 3]), Iter([1, 4, 7, 10]), Iter([2, 5]))
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 4
    >>> assert it.next() == 5
    >>> assert it.next() == 7
    >>> assert it.next() == 10
    >>> assert it.next() == None
    """
    sources: list[IterBase[T]]
    pos: int
    n: int

    def __init__(self, *sources: IterBase[T]):
        self.sources = sources
        self.pos = 0
        self.n = len(self.sources)

    def next(self) -> Optional[T]:
        for _ in range(self.n):
            if (n := self.sources[self.pos].next()) is not None:
                self.pos = (self.pos + 1) % self.n
                return n
            else:
                self.pos = (self.pos + 1) % self.n
        return None

class Intersperse[T](IterBase[T]):
    """
    Insert a constant value between each element of an iterator.

    >>> it = Iter(range(3)).intersperse(-1)
    >>> assert it.next() == 0
    >>> assert it.next() == -1
    >>> assert it.next() == 2
    >>> assert it.next() == -1
    >>> assert it.next() == 2
    >>> assert it.next() == None
    """
    source: IterBase[T]
    elem: T
    # this list will only ever have ≤1 item; it's used in place of Rust's
    # Option<Option<T>> because in Python Optional[Optional[T]] is
    # indistinguishable from Optional[T]
    peek: list[Optional[T]]

    def __init__(self, elem: T, source: IterBase[T]):
        self.source = source
        self.elem = elem
        self.peek = list()

    def next(self) -> Optional[T]:
        if len(self.peek) < 1:
            self.peek.append(None)
            return self.source.next()
        elif self.peek == [None]:
            if (n := self.source.next()) is not None:
                self.peek.pop(0)
                self.peek.append(n)
                return self.elem
            else:
                return None
        else:
            n = self.peek.pop(0)
            self.peek.append(None)
            return n

class IntersperseWith[T](IterBase[T]):
    """
    Like `Intersperse`, but using a generating function instead of a concrete
    value.

    >>> external_state = range(1, 10)
    >>> it = Iter(range(3)).intersperse_with(lambda: -next(external_state))
    >>> assert it.next() == 0
    >>> assert it.next() == -1
    >>> assert it.next() == 1
    >>> assert it.next() == -2
    >>> assert it.next() == 2
    >>> assert it.next() == None
    """
    source: IterBase[T]
    gen: Callable[[], T]
    # this list will only ever have ≤1 item; it's used in place of Rust's
    # Option<Option<T>> because in Python Optional[Optional[T]] is
    # indistinguishable from Optional[T]
    peek: list[Optional[T]]

    def __init__(self, gen: Callable[[], T], source: IterBase[T]):
        self.source = source
        self.gen = gen
        self.peek = list()

    def next(self) -> Optional[T]:
        if len(self.peek) < 1:
            self.peek.append(None)
            return self.source.next()
        elif self.peek == [None]:
            if (n := self.source.next()) is not None:
                self.peek.pop(0)
                self.peek.append(n)
                return (self.gen)()
            else:
                return None
        else:
            n = self.peek.pop(0)
            self.peek.append(None)
            return n

class Map[T, U](IterBase[U]):
    """
    Apply a mapping function to each element.

    >>> it = Iter(range(5)).map(lambda n: chr(97 + n))
    >>> assert it.next() == "a"
    >>> assert it.next() == "b"
    >>> assert it.next() == "c"
    >>> assert it.next() == "d"
    >>> assert it.next() == "e"
    >>> assert it.next() == None
    """
    func: Callable[[T], U]
    source: IterBase[T]

    def __init__(self, func: Callable[[T], U], source: IterBase[T]):
        self.func = func
        self.source = source

    def next(self) -> Optional[U]:
        if (n := self.source.next()) is not None:
            return self.func(n)
        else:
            return None

class MapWhile[T, U](IterBase[U]):
    """
    Like `Map`, but only yield mapped values until the first `None` is
    encountered in the output.

    >>> from math import sqrt
    >>> it = (
    >>>     Iter([0, 1, 2, -2, -1, 5])
    >>>     .map_while(lambda n: sqrt(n) if n >= 0 else None)
    >>> )
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == sqrt(2)
    >>> assert it.next() == None
    """
    func: Callable[[T], Optional[U]]
    source: IterBase[T]
    yielding: bool

    def __init__(self, func: Callable[[T], Optional[U]], source: IterBase[T]):
        self.func = func
        self.source = source
        self.yielding = True

    def next(self) -> Optional[U]:
        if self.yielding:
            if (n := self.source.next()) is not None:
                if (m := self.func(n)) is not None:
                    return m
                else:
                    self.yielding = False
                    return None
            else:
                return None
        else:
            return None

class MultiCartesianProduct[T](IterBase[tuple[T, ...]]):
    """
    Like `CartesianProduct`, but iterates over the product of one *or more*
    subiterators. Only makes sense when the iterator element type is a subclass
    of `IterBase`.

    >>> it = (
    >>>     Iter(range(1, 4))
    >>>     .map(lambda n: Iter(range(n)))
    >>>     .multi_cartesian_product()
    >>> )
    >>> assert it.next() == (0, 0, 0)
    >>> assert it.next() == (0, 0, 1)
    >>> assert it.next() == (0, 0, 2)
    >>> assert it.next() == (0, 1, 0)
    >>> assert it.next() == (0, 1, 1)
    >>> assert it.next() == (0, 1, 2)
    >>> assert it.next() == None
    """
    source: Iter[tuple[T, ...]]

    def __init__(self, *sources: IterBase[T]):
        self.source = Iter(product(*sources))

    def next(self) -> Optional[tuple[T, ...]]:
        return self.source.next()

class Peekable[T](IterBase[T]):
    """
    Convert an iterator into one that has an extra `peek` method. `peek` returns
    the next element in the iterator (or `None`), but does not advance beyond
    that point.

    >>> it = Iter(range(4)).peekable()
    >>> assert it.peek() == 0 # calls to `peek` do not advance the iterator with
    >>> assert it.next() == 0 # regard to `next`
    >>> assert it.peek() == 1 # repeated calls to `peek` will return the same
    >>> assert it.peek() == 1 # value until `next` is called
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.peek() == None
    >>> assert it.next() == None
    """
    source: IterBase[T]
    peeked: Optional[T]

    def __init__(self, source: IterBase[T]):
        self.source = source
        self.peeked = None

    def next(self) -> Optional[T]:
        if self.peeked is not None:
            p = self.peeked
            self.peeked = None
            return p
        elif (n := self.source.next()) is not None:
            self.peeked = None
            return n
        else:
            return None

    def peek(self) -> Optional[T]:
        """
        Return the next element in the iterator, but do not advance iteration.
        """
        if self.peeked is not None:
            return self.peeked
        elif (n := self.source.next()) is not None:
            self.peeked = n
            return n
        else:
            return None

class Permutations[T](IterBase[tuple[T, ...]]):
    """
    Iterate over all permutations of all fixed-size subsequences of an iterator.

    >>> it = Iter(range(4)).permutations(2)
    >>> assert it.next() == (0, 1)
    >>> assert it.next() == (0, 2)
    >>> assert it.next() == (0, 3)
    >>> assert it.next() == (1, 0)
    >>> assert it.next() == (1, 2)
    >>> assert it.next() == (1, 3)
    >>> assert it.next() == (2, 0)
    >>> assert it.next() == (2, 1)
    >>> assert it.next() == (2, 3)
    >>> assert it.next() == (3, 0)
    >>> assert it.next() == (3, 1)
    >>> assert it.next() == (3, 2)
    >>> assert it.next() == None
    """
    source: Iter[tuple[T, ...]]

    def __init__(self, source: IterBase[T], size: Optional[int]=None):
        self.source = Iter(permutations(source, r=size))

    def next(self) -> Optional[tuple[T, ...]]:
        return self.source.next()

class Positions[T](IterBase[int]):
    """
    Like `Filter`, but yielding only the positions of elements satisfying a
    predicate.

    >>> it = Iter("hello world").positions(lambda c: ord(c) % 2 == 0)
    >>> assert it.next() == 0
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 5
    >>> assert it.next() == 8
    >>> assert it.next() == 9
    >>> assert it.next() == 10
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    source: IterBase[T]
    count: int

    def __init__(self, pred: Callable[[T], bool], source: IterBase[T]):
        self.pred = pred
        self.source = source
        self.count = 0

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if self.pred(n):
                k = self.count
                self.count += 1
                return k
            else:
                self.count += 1
                continue
        return None

class Scan[T, S, U](IterBase[U]):
    """
    Perform an accumulating operation as an iteration over intermediate states.
    This iterator takes an initial state value, and a function that computes a
    new state and a value, or returnes `None` to end iteration early. If the
    result is not `None`, the new state is used in the computation of the next
    item in the iterator. Only the computed value is yielded.

    >>> # simple "game" that accumulates a score based on a stream of input
    >>> # tokens; the game's state is defined by a switch and a score, evolving
    >>> # according to the following function:
    >>>
    >>> type State = tuple[bool, int]
    >>>
    >>> def step(state: State, token: str) -> Optional[tuple[State, int]]:
    >>>     match (state, token):
    >>>         case ((True, score), "a"):
    >>>             new_state = (True, score + 1)
    >>>             return (new_state, new_state[1])
    >>>         case ((True, score), "b"):
    >>>             new_state = (True, score - 1)
    >>>             return (new_state, new_state[1])
    >>>         case ((switch, score), "c"):
    >>>             new_state = (not switch, score)
    >>>             return (new_state, new_state[1])
    >>>         case (_, "z"):
    >>>             return None
    >>>         case _:
    >>>             return (state, state[1])
    >>>             
    >>> it = Iter("abcaddaacbbcbzcbaac").scan(1, step)
    >>> assert it.next() == 0    # "a"
    >>> assert it.next() == 0    # "b"
    >>> assert it.next() == 0    # "c"
    >>> assert it.next() == 1    # "a"
    >>> assert it.next() == 1    # "d"
    >>> assert it.next() == 1    # "d"
    >>> assert it.next() == 2    # "a"
    >>> assert it.next() == 3    # "a"
    >>> assert it.next() == 3    # "c"
    >>> assert it.next() == 3    # "b"
    >>> assert it.next() == 3    # "b"
    >>> assert it.next() == 3    # "c"
    >>> assert it.next() == 2    # "b"
    >>> assert it.next() == None # "z"
    """
    state: S
    func: Callable[[S, T], Optional[tuple[S, U]]]
    source: IterBase[T]

    def __init__(
        self,
        init: S,
        func: Callable[[S, T], Optional[tuple[S, U]]],
        source: IterBase[T],
    ):
        self.state = init
        self.func = func
        self.source = source

    def next(self) -> Optional[U]:
        if (n := self.source.next()) is not None:
            if (sr := self.func(self.state, n)) is not None:
                (s, r) = sr
                self.state = s
                return r
            else:
                return None
        else:
            return None

class Skip[T](IterBase[T]):
    """
    Discard the some number of an iterator's initial elements.

    Note that these elements must still be computed (which happens on the first
    call to `next` in the returned iterator); they only won't be yielded to the
    returned iterator.

    >>> it = Iter(range(10)).skip(5)
    >>> assert it.next() == 5
    >>> assert it.next() == 6
    >>> assert it.next() == 7
    >>> assert it.next() == 8
    >>> assert it.next() == 9
    >>> assert it.next() == None
    """
    n: int
    source: IterBase[T]
    skipped: bool

    def __init__(self, n: int, source: IterBase[T]):
        self.n = n
        self.source = source
        self.skipped = False

    def next(self) -> Optional[T]:
        if not self.skipped:
            for _ in range(self.n):
                self.source.next()
        return self.source.next()

class SkipWhile[T](IterBase[T]):
    """
    Discard the leading elements of an iterator that satisfy a predicate,
    yielding the first element that doesn't and all those remaining.

    >>> it = Iter("abcdddcba").skip_while(lambda c: c <= "c")
    >>> assert it.next() == "d"
    >>> assert it.next() == "d"
    >>> assert it.next() == "d"
    >>> assert it.next() == "c"
    >>> assert it.next() == "b"
    >>> assert it.next() == "a"
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    source: IterBase[T]
    skipped: bool

    def __init__(self, pred: Callable[[T], bool], source: IterBase[T]):
        self.pred = pred
        self.source = source
        self.skipped = False

    def next(self) -> Optional[T]:
        while not self.skipped:
            if (n := self.source.next()) is not None:
                if not self.pred(n):
                    self.skipped = True
                    return n
            else:
                return None
        return self.source.next()

class SplitBy[T](IterBase[list[T]]):
    """
    Like `SplitOn`, but using a given predicate to determine where to split the
    source iterator.

    >>> it = Iter(range(11)).split_by(lambda n: n % 3 == 0, incl=True)
    >>> assert it.next() == [0]
    >>> assert it.next() == [1, 2, 3]
    >>> assert it.next() == [4, 5, 6]
    >>> assert it.next() == [7, 8, 9]
    >>> assert it.next() == [10]
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    incl: bool
    source: IterBase[T]
    split: list[T]

    def __init__(
        self,
        pred: Callable[[T], bool],
        source: IterBase[T],
        incl: bool=False,
    ):
        self.pred = pred
        self.source = source
        self.incl = incl
        self.split = list()

    def next(self) -> Optional[list[T]]:
        while (n := self.source.next()) is not None:
            if self.pred(n):
                if self.incl:
                    self.split.append(n)
                split = self.split
                self.split = list()
                return split
            else:
                self.split.append(n)
        if len(self.split) > 0:
            split = self.split
            self.split = list()
            return split
        else:
            return None

class SplitOn[T](IterBase[list[T]]):
    """
    Split an iterator into groups based on the occurrance of a "target" iterator
    element. When iteration encounters an instance equal to the target, a group
    is yielded as a list. Elements remaining in the iterator after the final
    split are also yielded. Pass `incl=True` to include the matched element at
    the end of each group.

    The iterator element type must implement `__eq__`.

    >>> it = Iter("hello world\ngoodbye world").split_on("\n")
    >>> assert it.next() == "hello world"
    >>> assert it.next() == "goodbye world"
    >>> assert it.next() == None
    """
    elem: T
    incl: bool
    source: IterBase[T]
    split: list[T]

    def __init__(self, elem: T, source: IterBase[T], incl: bool=False):
        self.elem = elem
        self.source = source
        self.incl = incl
        self.split = list()

    def next(self) -> Optional[list[T]]:
        while (n := self.source.next()) is not None:
            if n == self.elem:
                if self.incl:
                    self.split.append(n)
                split = self.split
                self.split = list()
                return split
            else:
                self.split.append(n)
        if len(self.split) > 0:
            split = self.split
            self.split = list()
            return split
        else:
            return None

class StepBy[T](IterBase[T]):
    """
    Take only every `stepsize`-th element of an iterator. Note that *all
    discarded elements must still be computed*, and that the first element of
    the iterator will always be yielded, regardless of the step size.

    >>> it = Iter("hello world").step_by(3)
    >>> assert it.next() == "h"
    >>> assert it.next() == "l"
    >>> assert it.next() == "w"
    >>> assert it.next() == "l"
    >>> assert it.next() == None
    """
    stepsize: int
    source: IterBase[T]

    def __init__(self, stepsize: int, source: IterBase[T]):
        if stepsize < 1:
            raise ValueError(f"StepBy: expected step size > 0, got {stepsize}")
        self.stepsize = stepsize
        self.source = source

    def next(self) -> Optional[T]:
        if (n := self.source.next()) is not None:
            for _ in range(self.stepsize - 1):
                self.source.next()
            return n
        else:
            return None

class Take[T](IterBase[T]):
    """
    Take only the first `n` elements of an iterator.

    >>> it = Iter(range(10)).take(5)
    >>> assert it.next() == 0
    >>> assert it.next() == 1
    >>> assert it.next() == 2
    >>> assert it.next() == 3
    >>> assert it.next() == 4
    >>> assert it.next() == None
    """
    source: IterBase[T]
    remaining: int

    def __init__(self, n: int, source: IterBase[T]):
        self.remaining = n
        self.source = source

    def next(self) -> Optional[T]:
        if self.remaining > 0:
            if (n := self.source.next()) is not None:
                self.remaining -= 1
                return n
            else:
                return None
        else:
            return None

class TakeWhile[T](IterBase[T]):
    """
    Take only the leading elements of an iterator that satisfy a predicate.

    >>> it = Iter("abcdddcba").take_while(lambda c: c <= "c")
    >>> assert it.next() == "a"
    >>> assert it.next() == "b"
    >>> assert it.next() == "c"
    >>> assert it.next() == None
    """
    pred: Callable[[T], bool]
    source: IterBase[T]
    yielding: bool

    def __init__(self, pred: Callable[[T], bool], source: IterBase[T]):
        self.pred = pred
        self.source = source
        self.yielding = True

    def next(self) -> Optional[T]:
        if self.yielding:
            if (n := self.source.next()) is not None:
                if self.pred(n):
                    return n
                else:
                    self.yielding = False
                    return None
            else:
                return None
        else:
            return None

class Unique[T](IterBase[T]):
    """
    Discard all elements of an iterator that have occurred at least once before
    in the iterator.

    >>> it = Iter("abbcaeffd").unique()
    >>> assert it.next() == "a"
    >>> assert it.next() == "b"
    >>> assert it.next() == "c"
    >>> assert it.next() == "e"
    >>> assert it.next() == "f"
    >>> assert it.next() == "d"
    >>> assert it.next() == None
    """
    source: IterBase[T]
    seen: list[T]

    def __init__(self, source: IterBase[T]):
        self.source = source
        self.seen = list()

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if n in self.seen:
                continue
            else:
                self.seen.append(n)
                return n
        return None

class UniqueBy[T](IterBase[T]):
    """
    Like `Unique`, but using a given comparison function.

    >>> it = Iter([1, 4, 8, 5, 4, 3]).unique_by(lambda x, y: x % 3 == y % 3)
    >>> assert it.next() == 1
    >>> assert it.next() == 8
    >>> assert it.next() == 3
    >>> assert it.next() == None
    """
    func: Callable[[T, T], bool]
    source: IterBase[T]
    seen: list[T]

    def __init__(self, func: Callable[[T, T], bool], source: IterBase[T]):
        self.func = func
        self.source = source
        self.seen = list()

    def next(self) -> Optional[T]:
        while (n := self.source.next()) is not None:
            if any(self.func(n, x) for x in self.seen):
                continue
            else:
                self.seen.append(n)
                return n
        return None

class Windows[T](IterBase[list[T]]):
    """
    Iterate over rolling "windows" of iterator elements as lists of length *at
    most* some fixed window length. At the head and tail ends of the returned
    iterator, yielded lists will in general have lengths less than the window
    length.

    >>> it = Iter(range(5)).windows(3)
    >>> assert it.next() == [0]
    >>> assert it.next() == [0, 1]
    >>> assert it.next() == [0, 1, 2]
    >>> assert it.next() == [1, 2, 3]
    >>> assert it.next() == [2, 3, 4]
    >>> assert it.next() == [3, 4]
    >>> assert it.next() == [4]
    >>> assert it.next() == None
    """
    windowlen: int
    source: IterBase[T]
    window: list[T]

    def __init__(self, windowlen: int, source: IterBase[T]):
        if windowlen < 1:
            raise ValueError(
                f"Windows: expected window length > 0, got {windowlen}")
        self.windowlen = windowlen
        self.source = source
        self.window = list()

    def next(self) -> Optional[list[T]]:
        if (n := self.source.next()) is not None:
            self.window.append(n)
            if len(self.window) > self.windowlen:
                self.window.pop(0)
            return self.window.copy()
        elif len(self.window) > 1:
            self.window.pop(0)
            return self.window.copy()
        else:
            return None

class WindowsExact[T](IterBase[list[T]]):
    """
    Like `Windows`, but yielding only windows of *exactly* the window length.

    >>> Iter(range(5)).windows_exact(3)
    >>> assert it.next() == [0, 1, 2]
    >>> assert it.next() == [1, 2, 3]
    >>> assert it.next() == [2, 3, 4]
    >>> assert it.next() == None
    """
    windowlen: int
    source: IterBase[T]
    window: list[T]

    def __init__(self, windowlen: int, source: IterBase[T]):
        if windowlen < 1:
            raise ValueError(
                f"WindowsExact: expected window length > 0, got {windowlen}")
        self.windowlen = windowlen
        self.source = source
        self.window = list()

    def next(self) -> Optional[list[T]]:
        while len(self.window) < self.windowlen:
            if (n := self.source.next()) is not None:
                self.window.append(n)
            else:
                return None
        m = self.window.copy()
        self.window.pop(0)
        return m

class Zip[T, U](IterBase[tuple[T, U]]):
    """
    Combine two iterators into a single one that iterates over both in
    lock-step, yielding tuples of elements from both and ending whenever the
    shortest of the two runs out.

    >>> it = Iter(range(5)).zip(Iter("abc"))
    >>> assert it.next() == (0, "a")
    >>> assert it.next() == (1, "b")
    >>> assert it.next() == (2, "c")
    >>> assert it.next() == None
    """
    source_l: IterBase[T]
    source_r: IterBase[U]
    yielding: bool

    def __init__(self, source_l: IterBase[T], source_r: IterBase[U]):
        self.source_l = source_l
        self.source_r = source_r
        self.yielding = True

    def next(self) -> Optional[tuple[T, U]]:
        if self.yielding:
            n = self.source_l.next()
            m = self.source_r.next()
            if (n is not None) and (m is not None):
                return (n, m)
            else:
                self.yielding = False
                return None
        else:
            return None

class ZipLongest[T, U](IterBase[tuple[Optional[T], Optional[U]]]):
    """
    Like `Zip`, but ending only when both iterators have run out of elements.
    The yielded tuples may contain `None` in either position if either iterator
    is shorter than the other.

    >>> it = Iter(range(5)).zip_longest(Iter("abc"))
    >>> assert it.next() == (0, "a")
    >>> assert it.next() == (1, "b")
    >>> assert it.next() == (2, "c")
    >>> assert it.next() == (3, None)
    >>> assert it.next() == (4, None)
    >>> assert it.next() == None
    """
    source_l: IterBase[T]
    source_r: IterBase[U]

    def __init__(self, source_l: IterBase[T], source_r: IterBase[U]):
        self.source_l = source_l
        self.source_r = source_r

    def next(self) -> Optional[tuple[Optional[T], Optional[U]]]:
        n = self.source_l.next()
        m = self.source_r.next()
        if (n is not None) or (m is not None):
            return (n, m)
        else:
            return None

