# lablib

This is just a collection of useful pieces of code, with level of generality
somewhere between a simple analysis script and a larger control library.

## Making contributions
To contribute to this collection, create a new branch so that `main` doesn't get
polluted with in-progress projects. When you're ready to submit your
contribution, create a merge request so that we can all review the code and
offer suggestions.

